Agave Platform MongoDB Docker Image
====================

Base docker image to bootstrap and run a MongoDB database server based on the [Tutum Docker MongoDB](https://github.com/tutumcloud/tutum-docker-mongodb) project. For additional sync and
backup capabilities, see the [Agave MongoDB Sync](https://bitbucket.org/taccaci/mongodb-sync) project.


MongoDB version
---------------

Different versions are built from different folders. If you want to use MongoDB, please check our `agaveapi/mongodb` image: https://github.com/agaveapi/mongodb


Usage
-----

To create the image `tutum/mongodb`, execute the following command on the tutum-mongodb folder:

        docker build -t agaveapi/mongodb 2.6/ .


Running the MongoDB server
--------------------------

Run the following command to start MongoDB:

        docker run -d -p 27017:27017 -p 28017:28017 agaveapi/mongodb

The first time that you run your container, two users will be created. An
admin user will be created and assigned root permission to the `admin` db. A
new random password will be created and assigned to this user.

A second user will be created and granted `dbOwner` permission on the `api`
database. The credentials for the second user are: `agaveuser` / `password`.

To get the login information for both users, check the logs of the container
by running:

        docker logs <CONTAINER_ID>

You will see an output like the following:

        ========================================================================
        You can now connect to this MongoDB server using:

            mongo admin -u admin -p 5elsT6KtjrqV --host <host> --port <port>

            or

            mongo api -u agaveuser -p password --host <host> --port <port>

        Please remember to change the above password as soon as possible!
        ========================================================================

In this case, `5elsT6KtjrqV` is the password set.
You can then connect to MongoDB:

         mongo admin -u admin -p 5elsT6KtjrqV

         or

         mongo api -u agaveuser -p password

Done!


Setting a specific password for the admin account
-------------------------------------------------

If you want to use a preset password instead of a randomly generated one, you can
set the environment variable `MONGODB_ADMIN_PASS` to your specific password when running the container:

        docker run -d -p 27017:27017 -p 28017:28017 -e MONGODB_ADMIN_PASS="mypass" agaveapi/mongodb

You can now test your new admin password:

        mongo admin -u admin -p mypass
        curl --user admin:mypass --digest http://localhost:28017/

Setting a specific alternate user and database
-------------------------------------------------

If you want to configure and initialize an alternative user or database, you can
set the environment variables `MONGODB_USERNAME`, `MONGODB_PASSWORD`, and
`MONGODB_DATABASE` when running the container:

        docker run -d -p 27017:27017 -p 28017:28017 \
                   -e MONGODB_ADMIN_PASS="mypass" \
                   -e MONGODB_USERNAME="jdoe" \
                   -e MONGODB_PASSWORD="changeit" \
                   -e MONGODB_DATABASE="mydb" \
                   agaveapi/mongodb

You can now test your new admin password:

        mongo mydb -u jdoe -p changeit
        curl --user admin:mypass --digest http://localhost:28017/


Run MongoDB without password
----------------------------

If you want to run MongoDB without password you can set the environment variable `AUTH` to specific if you want password or not when running the container:

        docker run -d -p 27017:27017 -p 28017:28017 -e AUTH=no agaveapi/mongodb

By default is "yes".


Run MongoDB with a specific storage engine
------------------------------------------

In MongoDB 3.0 there is a new environment variable `STORAGE_ENGINE` to specific the mongod storage driver:

        docker run -d -p 27017:27017 -p 28017:28017 -e AUTH=no -e STORAGE_ENGINE=mmapv1 agaveapi/mongodb:3.0

By default is "wiredTiger".


Change the default oplog size
-----------------------------

In MongoDB 3.0 the variable `OPLOG_SIZE` can be used to specify the mongod oplog size in megabytes:

        docker run -d -p 27017:27017 -p 28017:28017 -e AUTH=no -e OPLOG_SIZE=50 agaveapi/mongodb:3.0

By default MongoDB allocates 5% of the available free disk space, but will always allocate at least 1 gigabyte and never more than 50 gigabytes.


**by http://www.tutum.co, http://agaveapi.co**
